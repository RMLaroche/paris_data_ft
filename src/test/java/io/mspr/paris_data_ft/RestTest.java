package io.mspr.paris_data_ft;

import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class RestTest {

    @Test
    public void getResponseCodeTest() throws IOException {
        URL url = new URL("http://parisdata.rlaroche.fr/api/systemeInformation");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        assertEquals(200, connection.getResponseCode());
    }
}
