package io.mspr.paris_data_ft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebTest {

	public WebDriver driver;

	@Before
	public void setUp() {
			System.out.println("Demarage des scenarios sur une machine " + System.getProperty("os.name"));

			ChromeOptions options = new ChromeOptions();
			options.addArguments("--test-type");
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless");
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + new File("/chromedriver"));
			driver = new ChromeDriver(options);
			System.out.println("Automation Driver is " + driver);
		}

	@Test
	public void titleTest() {
		System.out.println("########################## Test Case Execution Started #########################");
		driver.get("http://parisdataweb.rlaroche.fr/mvc/view/frame.php");
		assertEquals("HTML Frames", driver.getTitle());
		System.out.println("########################## Test Case Execution Completed #########################");
	}
	

		@After
		public void tearDown() {
			driver.quit();
		}
}
